<?php
class productsModel{
    public $conexion;
    public function __construct(){
        $this->conexion = new mysqli('localhost','root','','jugadores');
        mysqli_set_charset($this->conexion,'utf8');
    }

    public function getProducts($id=null){
        $where = ($id == null) ? "" : " WHERE id='$id'";
        $products=[];
        $sql="SELECT * FROM jugadores ".$where;
        $registos = mysqli_query($this->conexion,$sql);
        while($row = mysqli_fetch_assoc($registos)){
            array_push($products,$row);
        }
        return $products;
    }

    public function saveProducts($jugador,$edad,$pais,$equipo,$posicion){
        $valida = $this->validateProducts($jugador,$edad,$pais,$equipo,$posicion);
        $resultado=['error','Ya existe un jugador con las mismas características'];
        if(count($valida)==0){
            $sql="INSERT INTO jugadores(jugador,edad,pais,equipo,posicion) VALUES('$jugador','$edad','$pais','$equipo','$posicion')";
            mysqli_query($this->conexion,$sql);
            $resultado=['success','Jugador guardado'];
        }
        return $resultado;
    }

    public function updateProducts($id,$jugador,$edad,$pais,$equipo,$posicion){
        $existe= $this->getProducts($id);
        $resultado=['error','No existe el jugador con ID '.$id];
        if(count($existe)>0){
            $valida = $this->validateProducts($jugador,$edad,$pais,$equipo,$posicion);
            $resultado=['error','Ya existe un jugador las mismas características'];
            if(count($valida)==0){
                $sql="UPDATE jugadores SET jugador='$jugador', edad='$edad', pais='$pais', equipo='$equipo', posicion='$posicion' WHERE id='$id' ";
                mysqli_query($this->conexion,$sql);
                $resultado=['success','Jugador actualizado'];
            }
        }
        return $resultado;
    }
    
    public function deleteProducts($id){
        $valida = $this->getProducts($id);
        $resultado=['error','No existe el jugador con ID '.$id];
        if(count($valida)>0){
            $sql="DELETE FROM jugadores WHERE id='$id' ";
            mysqli_query($this->conexion,$sql);
            $resultado=['success','Jugador eliminado'];
        }
        return $resultado;
    }
    
    public function validateProducts($jugador,$edad,$pais,$equipo,$posicion){
        $products=[];
        $sql="SELECT * FROM jugadores WHERE jugador='$jugador' AND edad='$edad' AND pais='$pais' AND equipo='$equipo' AND posicion='$posicion' ";
        $registos = mysqli_query($this->conexion,$sql);
        while($row = mysqli_fetch_assoc($registos)){
            array_push($products,$row);
        }
        return $products;
    }
}