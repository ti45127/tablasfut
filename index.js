$(function(){
    getProductos();
});

var url = 'http://localhost/controller/jugadoresController.php';

function getProductos(){
    $('#contenido').empty();
    $.ajax({
        type: 'GET',
        url: url,
        dataType: 'json',
        success: function(respuesta){
            var productos = respuesta;
            if(productos.length > 0){
                jQuery.each(productos,function(i,prod){
                    var btnEditar='<button class="btn btn-warning openModal edit-button" data-id="'+prod.id+'" data-jugador="'+prod.jugador+'" data-edad="'+prod.edad+'" data-pais="'+prod.pais+'" data-equipo="'+prod.equipo+'" data-posicion="'+prod.posicion+'" data-op="2" data-bs-toggle="modal" data-bs-target="#modalProductos"><i class="fa-solid fa-user-pen"></i></button>';
                    var btnEliminar='<button class="btn btn-danger delete" id="btnEliminar" data-id="'+prod.id+'" data-jugador="'+prod.jugador+'"><i class="fa-solid fa-trash"></i></button>';
                    $('#contenido').append('<tr><td>'+(i+1)+'</td><td>'+prod.jugador+'</td><td>'+prod.edad+'</td><td>'+prod.pais+'</td><td>'+prod.equipo+'</td><td>'+prod.posicion+'</td><td class="text-center">'+btnEditar+' '+btnEliminar+'</td></tr>');
                });
            }
        },
        error: function(){
            mostrar_alerta('Error al mostrar los jugadores','error');
        }
    });
}

$(document).on('click','#btnGuardar',function(){
    var id = $('#id').val();
    var jugador = $('#jugador').val().trim();
    var edad = $('#edad').val();
    var pais = $('#pais').val().trim();
    var equipo = $('#equipo').val().trim();
    var posicion = $('#posicion').val().trim();
    var opcion = $('#btnGuardar').attr('data-operacion');
    var metodo, parametros;
    if(opcion === '1'){
        metodo = 'POST';
        parametros = {jugador: jugador, edad: edad, pais: pais, equipo: equipo, posicion: posicion};
    }
    else{
        metodo = 'PUT';
        parametros = {id: id, jugador: jugador, edad: edad, pais: pais, equipo: equipo, posicion: posicion};
    }
    if(jugador === ''){
        mostrar_alerta('Escribe el nombre del jugador','warning','jugador');
    }
    else if(edad === ''){
        mostrar_alerta('Escribe la edad del jugador','warning','edad');
    }
    else if(pais === ''){
        mostrar_alerta('Escribe el país del jugador','warning','pais');
    }
    else if(equipo === ''){
        mostrar_alerta('Escribe el equipo del jugador','warning','equipo');
    }
    else if(posicion === ''){
        mostrar_alerta('Escribe la posicion del jugador','warning','posicion');
    }
    else{
        enviarSolicitud(metodo, parametros);
    }
});

$(document).on('click', '#btnEliminar', function(){ 
    var id = $(this).attr('data-id');
    var jugador = $(this).attr('data-jugador');
    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {confirmButton: 'btn btn-success ms-3', cancelButton:'btn btn-danger'},buttonsStyling:false
    });
    swalWithBootstrapButtons.fire({
        title: 'Estas seguro de eliminar al jugador: ' + jugador,
        text: 'Se perderá toda la información',
        icon: 'question',
        showCancelButton: true,
        confirmButtonText: 'Si, eliminar',
        cancelButtonText: 'Cancelar',
        reverseButtons: true
    }).then((result) => {
        if(result.isConfirmed){
            enviarSolicitud('DELETE', {id:id});
        }
        else{
            show_alerta('No se elimino al jugador','error');
        }
    });
});

function enviarSolicitud(metodo, parametros){
    $.ajax({
        type: metodo,
        url: url,
        data:JSON.stringify(parametros),
        dataType: 'json',
        success: function(respuesta){
            show_alerta(respuesta[1], respuesta[0]);
            if(respuesta[0] === 'success'){
                $('#btnCerrar').trigger('click');
                getProductos();
            }
        },
        error: function(){
            show_alerta('Error en la solicitud','error');
        }
    });
}

$(document).on('click', '.openModal', function(){
    limpiar();
    var opcion = $(this).attr('data-op');
    if(opcion == '1'){
        $('#titulo_modal').html('Registrar jugador');
        $('#btnGuardar').attr('data-operacion',1);
    }
    else{
        $('#titulo_modal').html('Editar jugador');
        $('#btnGuardar').attr('data-operacion',2);
        var id = $(this).attr('data-id');
        var jugador = $(this).attr('data-jugador');
        var edad = $(this).attr('data-edad');
        var pais = $(this).attr('data-pais');
        var equipo = $(this).attr('data-equipo');
        var posicion = $(this).attr('data-posicion');
        $('#id').val(id);
        $('#jugador').val(jugador);
        $('#edad').val(edad);
        $('#pais').val(pais);
        $('#equipo').val(equipo);
        $('#posicion').val(posicion);
    }
    window.setTimeout(function(){
        $('#jugador').trigger('focus');
    }, 500);
});

function limpiar(){
    $('#id').val('');
    $('#jugador').val('');
    $('#edad').val('');
    $('#pais').val('');
    $('#equipo').val('');
    $('#posicion').val('');
}

function show_alerta(mensaje,icono,foco){
    if(foco !==""){
        $('#'+foco).trigger('focus');
    }
    Swal.fire({
        title:mensaje,
        icon:icono,
        customClass: {confirmButton: 'btn btn-primary', popup:'animated zoomIn' },
        buttonsStyling:false
    });
}
