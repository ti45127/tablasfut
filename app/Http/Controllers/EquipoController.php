<?php

namespace App\Http\Controllers;

use App\Models\Equipo;
use Illuminate\Http\Request;

class EquipoController extends Controller
{
    public function index()
    {
        $equipos = Equipo::all();
        return view('index-equipos', compact('equipos'));
    }

    public function create()
    {
        return view('equipos_create');
    }

    public function store(Request $request)
    {
        // Lógica para almacenar un nuevo equipo
        Equipo::create($request->all());

        return redirect()->route('equipos.index')->with('success');
    }

    public function edit($id)
    {
        $equipo = Equipo::findOrFail($id);
        return view('equipos_edit', compact('equipo'));
    }

    public function update(Request $request, $id)
    {
        $equipo = Equipo::findOrFail($id);
        $equipo->update($request->all());

        return redirect()->route('equipos.index')->with('success');
    }

    public function destroy($id)
    {
        $equipo = Equipo::findOrFail($id);
        $equipo->delete();

        return redirect()->route('equipos.index')->with('success');
    }
}
