<?php

namespace App\Http\Controllers;

use App\Models\Jugador;
use App\Models\Equipo;  
use Illuminate\Http\Request;

class JugadorController extends Controller
{
    public function index()
    {
        $jugadores = Jugador::all();
        return view('index', compact('jugadores'));
    }

    public function create()
    {
        $equipos = Equipo::all();
        return view('jugadores_create', compact('equipos'));
    }

    public function store(Request $request)
{
    
    Jugador::create($request->all());

    $jugadores = Jugador::all(); 

    return view('index', compact('jugadores'))->with('success');
}

public function edit($id)
{
    $jugador = Jugador::findOrFail($id);
    $equipos = Equipo::all();
    return view('edit', compact('jugador', 'equipos'));
}

public function update(Request $request, $id)
{
    $jugador = Jugador::findOrFail($id);
    $jugador->update($request->all());
    return redirect()->route('jugadores.index')->with('success');
}

public function destroy($id)
{
    $jugador = Jugador::findOrFail($id);
    $jugador->delete();
    return redirect()->route('jugadores.index')->with('success');
}

}
