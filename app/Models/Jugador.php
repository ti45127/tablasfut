<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Jugador extends Model
{
    protected $table = 'jugadores'; // Agrega esta línea para especificar el nombre de la tabla

    protected $fillable = [
        'nombre',
        'edad',
        'posicion',
        'equipo_id', // Asegúrate de tener el campo asociado al equipo
    ];

    public function equipo()
    {
        return $this->belongsTo(Equipo::class);
    }

    // Resto de tu código
}
