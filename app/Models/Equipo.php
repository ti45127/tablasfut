<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Equipo extends Model
{
    protected $fillable = [
        'nombre',
        'ciudad',
        'fundacion_anio',
    ];

    public function jugadores()
    {
        return $this->hasMany(Jugador::class);
    }

    // Resto de tu código
}
