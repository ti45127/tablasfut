<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EquipoController;
use App\Http\Controllers\JugadorController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Rutas para mostrar y crear equipos
Route::get('/equipos', [EquipoController::class, 'index'])->name('equipos.index');
Route::get('/equipos/create', [EquipoController::class, 'create'])->name('equipos.create');
Route::post('/equipos/store', [EquipoController::class, 'store'])->name('equipos.store');
Route::get('/jugadores/{jugador}/edit', [JugadorController::class, 'edit'])->name('jugadores.edit');
Route::put('/jugadores/{jugador}', [JugadorController::class, 'update'])->name('jugadores.update');
Route::delete('/jugadores/{jugador}', [JugadorController::class, 'destroy'])->name('jugadores.destroy');
// Rutas para mostrar y crear jugadores
Route::get('/jugadores', [JugadorController::class, 'index'])->name('jugadores.index');
Route::get('/jugadores/create', [JugadorController::class, 'create'])->name('jugadores.create');
Route::post('/jugadores/store', [JugadorController::class, 'store'])->name('jugadores.store');
Route::get('/equipos/edit/{id}', [EquipoController::class, 'edit'])->name('equipos.edit');
Route::put('/equipos/update/{id}', [EquipoController::class, 'update'])->name('equipos.update');
Route::delete('/equipos/destroy/{id}', [EquipoController::class, 'destroy'])->name('equipos.destroy');
Route::get('/', function () {
    return view('welcome');
});
