<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Editar Equipo</title>

    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #fff;
            margin: 0;
            padding: 0;
            box-sizing: border-box;
        }

        h1 {
            color: #333;
            text-align: center;
            padding: 20px;
            margin: 0;
        }

        form {
            width: 80%;
            max-width: 600px;
            margin: 20px auto;
            background-color: #fff;
            padding: 20px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            border-radius: 5px;
        }

        label {
            display: block;
            margin-bottom: 8px;
        }

        input, select {
            width: 100%;
            padding: 10px;
            margin-bottom: 15px;
            box-sizing: border-box;
            border: 1px solid #ddd;
            border-radius: 5px;
            font-size: 16px;
        }

        button {
            background-color: #FF7E04;
            color: #fff;
            padding: 10px 20px;
            border: none;
            border-radius: 5px;
            font-size: 16px;
            cursor: pointer;
        }

        button:hover {
            background-color: #9B3B25;
        }
    </style>
</head>
<body>
    <h1>Editar Equipo</h1>

    <form action="{{ route('equipos.update', $equipo->id) }}" method="POST">
        @csrf
        @method('PUT')

        <label for="nombre">Nombre del Equipo:</label>
        <input type="text" name="nombre" value="{{ $equipo->nombre }}" required>

        <label for="ciudad">Ciudad:</label>
        <input type="text" name="ciudad" value="{{ $equipo->ciudad }}" required>

        <label for="fundacion_anio">Año de Fundación:</label>
        <input type="number" name="fundacion_anio" value="{{ $equipo->fundacion_anio }}" required>

        

        <button type="submit">Actualizar Equipo</button>
    </form>
</body>
</html>
