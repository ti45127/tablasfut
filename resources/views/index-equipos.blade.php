<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
    <title>Listado de Equipos</title>

    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #fff;
            margin: 0;
            padding: 0;
            box-sizing: border-box;
        }

        h1 {
            color: #333;
            text-align: center;
            padding: 20px;
            margin: 0;
        }

        table {
            width: 80%;
            margin: 20px auto;
            background-color: #fff;
        }

        th, td {
            padding: 15px;
            text-align: left;
            border-bottom: 1px solid #000000;
        }

        th {
            background-color: #fff;
            color: black;
        }

        tbody tr:hover {
            background-color: #f5f5f5;
        }

        .actions {
            display: flex;
            justify-content: space-around;
        }

        .edit,
        .delete {
            display: inline-block;
            padding: 8px;
            border: none;
            border-radius: 3px;
            cursor: pointer;
        }

        .edit {
            background-color: #FF7E04;
            color: #000000;
            margin-right: 5px;
        }

        .delete {
            background-color: #FF7E04;
            color: #fff;
        }

        a {
            display: inline-block;
            padding: 10px 20px;
            background-color: #FF7E04;
            color: #fff;
            text-decoration: none;
            border-radius: 5px;
            margin: 20px;
        }

        a:hover {
            background-color: #9B3B25;
        }

        p {
            color: green;
            text-align: center;
        }
    </style>
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="#">Equipos</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
              <a class="nav-link" href="http://127.0.0.1:8000/jugadores">Jugadores</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="http://127.0.0.1:8000/equipos">Equipos</a>
            </li>
        </div>
      </nav>
    <h1>Lista de Equipos</h1>
    <img src="/imagenes/estadio.jpg" alt="">

    @if(session('success'))
        <p style="color: green;">{{ session('success') }}</p>
    @endif

    <table>
        <thead>
            <tr>
                <th>ID</th>
                <th>Nombre</th>
                <th>Ciudad</th>
                <th>Año de Fundación</th>
                <th>Acciones</th>
                
            </tr>
        </thead>
        <tbody>
            @foreach($equipos as $equipo)
                <tr>
                    <td>{{ $equipo->id }}</td>
                    <td>{{ $equipo->nombre }}</td>
                    <td>{{ $equipo->ciudad }}</td>
                    <td>{{ $equipo->fundacion_anio }}</td>
                    <td class="actions">
                        <button class="edit" onclick="window.location='{{ route('equipos.edit', $equipo->id) }}'">Editar</button>
                        <button class="delete" onclick="if (confirm('¿Estás seguro de eliminar este equipo?')) { document.getElementById('delete-form-{{ $equipo->id }}').submit(); }">Eliminar</button>
                        <form id="delete-form-{{ $equipo->id }}" action="{{ route('equipos.destroy', $equipo->id) }}" method="POST" style="display: none;">
                            @csrf
                            @method('DELETE')
                        </form>
                    </td>
                    
                </tr>
            @endforeach
        </tbody>
    </table>

    <a href="{{ route('equipos.create') }}">Crear Nuevo Equipo</a>
</body>
</html>
