<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Crear Nuevo Jugador</title>

    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #fff;
            margin: 0;
            padding: 0;
            box-sizing: border-box;
        }

        h1 {
            color: #333;
            text-align: center;
            padding: 20px;
            margin: 0;
        }

        form {
            width: 80%;
            max-width: 600px;
            margin: 20px auto;
            background-color: #fff;
            padding: 20px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            border-radius: 5px;
        }

        label {
            display: block;
            margin-bottom: 8px;
        }

        input, select {
            width: 100%;
            padding: 10px;
            margin-bottom: 15px;
            box-sizing: border-box;
            border: 1px solid #ddd;
            border-radius: 5px;
            font-size: 16px;
        }

        button {
            background-color: #FF7E04;
            color: #fff;
            padding: 10px 20px;
            border: none;
            border-radius: 5px;
            font-size: 16px;
            cursor: pointer;
            
        }

        button:hover {
            background-color: #9B3B25;
        }
    </style>
</head>
<body>
    <h1>Crear Nuevo Jugador</h1>

    <form action="{{ route('jugadores.store') }}" method="POST">
        @csrf
        <label for="nombre">Nombre del Jugador:</label>
        <input type="text" name="nombre" required>

        <label for="edad">Edad:</label>
        <input type="number" name="edad" required>

        <label for="posicion">Posición:</label>
        <input type="text" name="posicion" required>

        <label for="equipo_id">Equipo:</label>
        <select name="equipo_id" required>
            @foreach($equipos as $equipo)
                <option value="{{ $equipo->id }}">{{ $equipo->nombre }}</option>
            @endforeach
        </select>

        

        <button type="submit">Guardar Jugador</button>
    </form>
</body>
</html>
